{ ... }:
{ config, pkgs, lib, ... }:

{
  home = {
    stateVersion = "18.09";
    sessionVariables = {
      LOCALE     = "de_DE.UTF8";
    };

    packages = with pkgs; [
      cachix
      cpufrequtils
      eza
      gitAndTools.git-extras
      git-absorb
      gnupg
      home-manager
      htop
      killall
      niv
      p7zip
      pciutils
      ripgrep
      signalbackup-tools
      silver-searcher
      stack
      stress
      unzip
      wget
      wgetpaste
      xsel
      zint
    ];
  };

  programs = {
    bash = {
      enable = true;
      historySize = 1000000;
    };

    git = {
      enable    = true;
      lfs.enable = true;
      userName  = "Manuel Bärenz";
      aliases = let
        csvRegex = "--word-diff-regex='[,;][\-\+0-9a-z_]*'";
      in {
        au = "add -u";
        bv = "branch -vvv --sort=-committerdate";
        ca = "commit --all";
        caa = "commit --amend --all";
        ci = "commit --interactive";
        cor = "commit --interactive -C ORIG_HEAD";
        diff-csv = "diff ${csvRegex}";
        fu = "remote update upstream";
        log-csv = "log -p ${csvRegex}";
        show-csv = "show ${csvRegex}";
        pf = "push --force-with-lease";
        pso = "!git push --set-upstream origin $(git branch --show-current)";
        r = "rebase origin/master --autostash";
        ra = "rebase --abort";
        rc = "rebase --continue";
        rca = "r --exec 'cabal build all --enable-tests --enable-benchmarks -fdev'";
        rce = "rebase origin/develop --autostash --exec 'cabal build all --enable-tests --enable-benchmarks --ghc-options=\"-Werror\"'";
        rfo = "rebase origin/develop --autostash --exec 'ormolu -i $(git show --name-only --format=\"\" -- **/*.hs); git commit -a --amend --no-edit'";
        rh = "rebase origin/develop --autostash --exec 'for file in $(git show --name-only --format=\"\" -- **/*.hs); do hlint --refactor --refactor-options=-i $file; done; git commit -a --amend --no-edit'";
        rf = "r -X theirs --exec 'fourmolu -i $(git ls-files \"*.hs\"); git commit -a --amend --no-edit'";
        ri = "r --interactive";
        rr = "for hash in $(git log ..origin/master  --reverse --pretty=format:%H); do git rebase $hash; read; done";
        rs = "rebase --skip";
        rsh = "reset --soft HEAD~1";
        ru = "rebase upstream/master --autostash";
        rui = "ru --interactive";
      };
      extraConfig = {
        color.ui = true;
        diff.wsErrorHighlight = "all";
        pull.rebase = true;
      };
      ignores = [
        "*.swp"
        "**/.stack-work/"
        "*.aux"
        "*.out"
        "*.log"
        "*.pyg"
        "*.pygstyle"
        "*.pygtex"
        "*.bbx"
        "*.bst"
        "*.cbx"
        "*.dbx"
        "*.bbl"
        "*.blg"
        "*.dvi"
        "*.fdb_latexmk"
        "*.fls"
        "*.xcp"
        "*.orig"
        "**/_minted*/"
        ".envrc"
        ".direnv/"
        "dist-newstyle"
      ];
    };

    mercurial = {
      enable    = true;
      userName  = "Manuel Bärenz";
      userEmail = "programming@manuelbaerenz.de";
    };

    nix-index = {
      enable = true;
      enableBashIntegration = true;
    };

    ssh = {
      enable = true;
      # It seems that sometimes gitlab has IPv6 problems
      extraConfig = ''
        Host gitlab.com
          AddressFamily inet
      '';
    };

    vim = {
      enable   = true;
      settings = {
        number = true;
        shiftwidth = 2;
        expandtab = true;
        background = "light";
      };
      plugins = with pkgs.vimPlugins; [
        ack-vim
        agda-vim
        airline
        fzfWrapper
        fzf-vim
        ctrlp
        commentary
        fugitive
        nerdtree
        nerdtree-git-plugin
        vim-sensible
        sleuth
        vim-addon-nix
        vim-elixir
        #vim-git
     ];
     extraConfig = ''
       set list
       set listchars=tab:->,trail:~,extends:>,precedes:<
       set colorcolumn=110
       nnoremap <F3> :GFiles<CR>
       nnoremap <F4> :FZF<CR>
       nnoremap <F5> :Buffers<CR>
       nnoremap <F9> :Ag<CR>
       nnoremap <F8> :NERDTree<CR>
       let g:airline#extensions#default#section_truncate_width = { 'b': 40 }
       set scrolloff=15
       '';
    };
  };
}
