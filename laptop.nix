inputs:
{ config, pkgs, lib, ... }:

{
  imports = [
    ./wifionice.nix # Temporary
    (inputs.nixpkgs-keyboardio + "/nixos/modules/hardware/keyboard/keyboardio.nix")
  ];

  nixpkgs.overlays = [
    (self: super: {
      inherit ((import inputs.nixpkgs-keyboardio { system = "x86_64-linux"; }).pkgs) keyboardio-udev-rules;
    })
  ];

  # So I can build nixpkgs & run tests on aarch64
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
  };

  networking = {
    networkmanager.enable = true;
  };

  services.fwupd.enable = true;

  services.printing = {
    enable = true;
    drivers = with pkgs; [
      gutenprint
      brlaser
      samsung-unified-linux-driver
    ];
    startWhenNeeded = false;
    logLevel = "debug";
  };

  # For network printer auto discovery
  services.avahi.enable = true;

  services.earlyoom = {
    enable = true;
  };

  services.pipewire = {
    pulse.enable = true;
    jack.enable = true;
    alsa.enable = true;
  };
  security.rtkit.enable = true; # For pipewire

  hardware.bluetooth.enable = true;

  hardware.keyboard.keyboardio.enable = true;

  # Enable the X11 windowing system.
  services.displayManager.sddm.enable = true;
  services.xserver = {
    enable = true;
    # xkb settings are in configuration.nix

    # Enable the KDE Desktop Environment.
    desktopManager.plasma5.enable = true;
    # Enable touchpad support.
  };
  services.libinput = {
    touchpad = {
      disableWhileTyping = true;
    };
    enable = true;
  };
  # services.xserver.xkbOptions = "eurosign:e";

  security.pam = {
    services.turion.enableKwallet = true;
  };

  users.users.turion = {
    extraGroups = [
      "audio"
    ];
  };

  home-manager = {
    sharedModules = [
      ./home/laptop.nix
    ];
  };

  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "samsung-UnifiedLinuxDriver"
  ];
}
