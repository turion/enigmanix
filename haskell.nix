{ pkgs, ... }:

with pkgs;
let
  ghc = haskell.packages.ghc96.ghcWithPackages (packages: with packages; [
    fourmolu
    gloss
    hlint
    haskell-language-server
    hmatrix
    HUnit
    implicit-hie
    MonadRandom
    PortMidi
    pulse-simple
    QuickCheck
    test-framework
    test-framework-hunit
    test-framework-quickcheck2
    vector
    warp
  ]);
in
{
  home.packages = [
    cabal2nix
    haskellPackages.cabal-fmt
    ghc
    ormolu
  ] ++ (with haskellPackages; [
    cabal-install
    eventlog2html
    cabal-gild
  ]);
}
