{
  description = "My system";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixpkgs-server.url = "github:NixOS/nixpkgs/nixos-24.11-small";
    nixpkgs-keyboardio.url = "github:turion/nixpkgs/dev_keyboardio";
    home-manager = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    manuelbaerenz = {
      url = "git+ssh://git@gitlab.com/turion/manuelbaerenz.de";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    dorotheabaerenz = {
      url = "git+ssh://git@gitlab.com/turion/dorotheabaerenz.de";
      flake = false;
    };
    gilgamesch-chor-epos = {
      url = "git+ssh://git@github.com/turion/gilgamesch-chor-epos";
    };
  };

  outputs = inputs@{
      nixpkgs,
      nixpkgs-server,
      home-manager,
      nixos-hardware,
      manuelbaerenz,
      dorotheabaerenz,
    ... }: {
    nixosConfigurations = let
      common = import ./configuration.nix inputs;
      laptop = import ./laptop.nix inputs;
      system = "x86_64-linux";
    in {
      thnixpad = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = [
          common
          home-manager.nixosModules.home-manager
          laptop
          ./specific/thnixpad
          nixos-hardware.nixosModules.lenovo-thinkpad-t440s
        ];
      };

      manuelbaerenz = nixpkgs-server.lib.nixosSystem {
        inherit system;
        modules = [
          common
          home-manager.nixosModules.home-manager
          (import ./specific/manuelbaerenz system inputs)
        ];
      };

      nixPie = nixpkgs.lib.nixosSystem {
        system = "aarch64-linux";
        modules = [
          nixos-hardware.nixosModules.raspberry-pi-4
          ./specific/nixPie
        ];
      };

      heilmann = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = [
          common
          home-manager.nixosModules.home-manager
          laptop
          ./specific/heilmann
        ];
      };
    };
  };
}
