# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

system:
{ dorotheabaerenz, manuelbaerenz, gilgamesch-chor-epos, nixpkgs-server, ... }:

{ config
, pkgs
, dorotheabaerenz-de
, ...
}:

let
  dorotheabaerenz-de = import (dorotheabaerenz + "/default.nix") {
    nixpkgs = import nixpkgs-server {
      inherit system;
    };
  };
in
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./cachix.nix
      ./mastodon.nix
      ./pipperlapupp.nix
      (import ./gilgamesch-chor-epos.nix gilgamesch-chor-epos)
      ./audiobookshelf.nix
      ./nextcloud.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.devices = [
    "/dev/sda"
    "/dev/sdb"
  ];

  boot.swraid.mdadmConf = ''
    MAILADDR manuel@enigmage.de
    '';

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    apacheHttpd
    cachix
    hdparm
    htop
    nethogs
    sqlite
    wget
    vim
    git
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
    settings = {
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = true;
      GatewayPorts = "yes";
    };
  };

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDIJkMehxRoUvMn/a1F6UJ1fHru56bmLMvcba7ECt2TMxznTstXOvp8l+/eu6Fj3b/SaWxcl/h9KBv1wrarUeqO3HNU9GNVv7W4bn8ze0Ecuimc08AaXYO3B91oLDrb3bblAFCamAKbmm0NKuFWNGfHiNSXzrpQuhTAy/wDQhDS33ccdffyGdT1YexczRhv3Q/c00onN2hXFApw1uFNVAjC39GHrYQakcrD06jFE624+eBWr0lxEOmtM1V0jR7n+FNSQ1DIZTZFOJddC7aydt8afnHAnqJA/GWE+4cS7qE34xzRB7HNZKX2wlb0FZChhdMwnRVFp6oeKszSOzeHr8bGic4a9KXb7nO1XsKb1guzIKYPtXbxWMk7/2Gq19r0F6VdF0NPxx0zOLBz7wIPuoxbV2oJ4pxI2ThTxRNhuYNtlUOgH+X1/Z2+6E9giOzCyXk2fy0r/ikrY+BO5YRmtYq63sZWQa+qdfHYS5yiZPLtpuBOMTMm2OQSZMj8pOa/Tls= turion@thnixpad" # Private laptop
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILkUTWinUH2q53GKcdHhdekcNs+Eax8d3c9yBcp4LeKm turion@nixos" # Heilmann work laptop
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMZgDoDWBD5ES6pyI06Td7KqUH1B4FJY2OY79NR1OjqN turion@thnixpad" # extra_key_for_server.pub
  ];

  networking = {
    hostName = "manuelbaerenz";
    firewall = {
      allowedTCPPorts = [ 80 443 ];
  # networking.firewall.allowedUDPPorts = [ ... ];
      allowPing = true;
    };

    defaultGateway = "89.238.82.1";
    nameservers = [
    	"8.8.8.8"
    	"8.8.4.4"
    ]; # Possibly change to local?
    interfaces.eth0 = { # Temporary because mainboard was changed
      ipv4.addresses = [
        {
          address = "89.238.82.209";
          prefixLength = 24;
        }
      ];
      ipv6.addresses = [
        {
          address = "fe80::fe93:abff:2cd8:dca5";
          prefixLength = 64;
        }
      ];
    };
    interfaces.enp2s0 = {
      ipv4.addresses = [
        {
          address = "89.238.82.209";
          prefixLength = 24;
        }
      ];
      ipv6.addresses = [
        {
          address = "fe80::fe93:abff:2cd8:dca5";
          prefixLength = 64;
        }
      ];
    };
  };

  services.nginx = {
    virtualHosts."www.manuelbaerenz.de" = {
      useACMEHost = "nextcloud.manuelbaerenz.de";
      forceSSL = true;
      locations."/essence-of-live-coding-files" = {
        root = "/root/essence-of-live-coding-files";
      };
      locations."/livecoding/" = {
        proxyPass = "http://localhost:8082/";
      };
      locations."/" = {
        proxyPass = "http://localhost:8080";
      };
    };
    virtualHosts."livecoding.manuelbaerenz.de" = {
      useACMEHost = "nextcloud.manuelbaerenz.de";
      locations."/" = {
        proxyPass = "http://localhost:8082";
      };
    };
    virtualHosts."manuelbaerenz.de" = {
      useACMEHost = "nextcloud.manuelbaerenz.de";
      forceSSL = true;
      extraConfig = "rewrite ^/(.*)$ https://www.manuelbaerenz.de/$1 redirect;";
    };
    virtualHosts."dorotheabaerenz.de" = {
      useACMEHost = "nextcloud.manuelbaerenz.de";
      forceSSL = true;
      extraConfig = "rewrite ^/(.*)$ https://www.dorotheabaerenz.de/$1 redirect;";
    };
    virtualHosts."www.dorotheabaerenz.de" = {
      useACMEHost = "nextcloud.manuelbaerenz.de";
      forceSSL = true;
      locations."/" = {
        root = "${dorotheabaerenz-de}";
        tryFiles = "$uri /index.html =404";
        extraConfig = ''
          auth_basic "Dorothea Bärenz";
          auth_basic_user_file /etc/nginx/htpasswd;
        '';
      };
    };
    virtualHosts."orga.dorotheabaerenz.de" = {
      useACMEHost = "nextcloud.manuelbaerenz.de";
      #forceSSL = true;
      locations."/" = {
        proxyPass = "https://cryptpad.fr/sheet/#/2/sheet/edit/iT0wVZDHz+Jsiy7b96gwAanH/";
      };
    };
  };

  security.acme.certs."nextcloud.manuelbaerenz.de".extraDomainNames = [
    "www.dorotheabaerenz.de"
    "dorotheabaerenz.de"
    "www.manuelbaerenz.de"
    "manuelbaerenz.de"
  ];
  security.acme = {
    defaults.email = "programming@manuelbaerenz.de";
    acceptTerms = true;
  };

  systemd.services."servant" = {
    description = "www.manuelbaerenz.de servant";
    serviceConfig = {
      Type = "simple";
      ExecStart = "${manuelbaerenz.packages.${system}.default}/bin/manuelbaerenz-de";
    };
    wantedBy = [ "default.target" ];
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "22.05"; # Did you read the comment?
}
