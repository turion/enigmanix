{ pkgs, ... }:

let
  datadir = "/var/lib/audiobookshelf";
  port = 8000;
  hostname = "podcast.pipperlapupp.de";
in
{
  services.audiobookshelf = {
    enable = true;
    package = pkgs.audiobookshelf;
    inherit port;
  };

  services.nginx = {
    enable = true;
    virtualHosts."${hostname}" = {
      locations."/" = {
        proxyPass = "http://localhost:${toString port}/";
      };
      enableACME = true;
      forceSSL = true;
    };
  };

  security.acme.certs."nextcloud.manuelbaerenz.de".extraDomainNames = [ hostname ];
}
