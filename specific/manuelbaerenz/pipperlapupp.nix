{ ... }:

{
  services.nginx = {
    virtualHosts."intern.pipperlapupp.de" = {
      useACMEHost = "nextcloud.manuelbaerenz.de";
      forceSSL = true;
      # locations."/" = {
      #   proxyPass = "https://nextcloud.manuelbaerenz.de/s/SECk8nDXDoZkfqt/";
      # };
      extraConfig = "rewrite ^/(.*)$ https://nextcloud.manuelbaerenz.de/s/SECk8nDXDoZkfqt redirect;";
      # extraConfig = "rewrite ^/(.*)$ https://nextcloud.manuelbaerenz.de/s/SECk8nDXDoZkfqt/$1 redirect;";
    };

    virtualHosts."pipperlapupp.de" = {
      useACMEHost = "nextcloud.manuelbaerenz.de";
      forceSSL = true;
      extraConfig = "rewrite ^/(.*)$ https://www.pipperlapupp.de/$1 redirect;";
    };

    virtualHosts."www.pipperlapupp.de" = {
      useACMEHost = "nextcloud.manuelbaerenz.de";
      forceSSL = true;
      extraConfig = "rewrite ^/(.*)$ https://pipperlapupp.wordpress.com/$1 redirect;";
      # locations."/" = {
      #   proxyPass = "https://pipperlapupp.wordpress.com/";
      # };
    };
  };

  security.acme.certs."nextcloud.manuelbaerenz.de".extraDomainNames = [
    "intern.pipperlapupp.de"
    "www.pipperlapupp.de"
    "pipperlapupp.de"
  ];
}
