
{
  nix.settings = {
    substituters = [
      "https://manuelbaerenz.cachix.org"
    ];
    trusted-public-keys = [
      "manuelbaerenz.cachix.org-1:bMYHIcG5JdUZ6+TdrXH4cR5PXRlVv1+EkPMu1Xi+QeU="
    ];
  };
}
