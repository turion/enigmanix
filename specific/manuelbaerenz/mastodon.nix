{ lib, config, pkgs, ... }:

let
  cfg = config.services.mastodon;
in
{
  services.mastodon = {
    enable = true;
    localDomain = "mastodon.gardensoul.org";
    configureNginx = true;
    smtp = {
      fromAddress = "admin@gardensoul.org";
    };
    mediaAutoRemove.olderThanDays = 7;
    streamingProcesses = 1;
  };

  services.postgresqlBackup = {
    enable = true;
    databases = [ "mastodon" ];
  };

  # Probably a good idea in general, but recommended by mastodon
  services.fail2ban.enable = true;
}
