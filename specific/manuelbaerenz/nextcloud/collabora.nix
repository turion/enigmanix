{ pkgs, ... }:

{
  # Copied from https://framagit.org/markas/infrastructure/-/blob/master/modules/nextcloud.nix 2024.09.04

  virtualisation.oci-containers = {
    # Since 22.05, the default driver is podman but it doesn't work
    # with podman. It would however be nice to switch to podman.
    backend = "docker";
    containers.collabora = {
      image = "collabora/code";
      imageFile = pkgs.dockerTools.pullImage {
        imageName = "collabora/code";
        imageDigest = "sha256:aab41379baf5652832e9237fcc06a768096a5a7fccc66cf8bd4fdb06d2cbba7f";
        sha256 = "sha256-M66lynhzaOEFnE15Sy1N6lBbGDxwNw6ap+IUJAvoCLs=";
      };
      ports = ["9980:9980"];
      environment = {
        domain = "nextcloud.manuelbaerenz.de";
        extra_params = "--o:ssl.enable=false --o:ssl.termination=true";
      };
      extraOptions = ["--cap-add" "MKNOD"];
    };
  };

  services.nginx.virtualHosts."office.manuelbaerenz.de" = {
    forceSSL = true;
    useACMEHost = "nextcloud.manuelbaerenz.de";
    locations = {
      # static files
      "^~ /loleaflet" = {
        proxyPass = "http://localhost:9980";
        extraConfig = ''
          proxy_set_header Host $host;
        '';
      };
      # WOPI discovery URL
      "^~ /hosting/discovery" = {
        proxyPass = "http://localhost:9980";
        extraConfig = ''
          proxy_set_header Host $host;
        '';
      };

      # Capabilities
      "^~ /hosting/capabilities" = {
        proxyPass = "http://localhost:9980";
        extraConfig = ''
          proxy_set_header Host $host;
        '';
      };

      # download, presentation, image upload and websocket
      "~ ^/lool" = {
        proxyPass = "http://localhost:9980";
        extraConfig = ''
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection "Upgrade";
          proxy_set_header Host $host;
          proxy_read_timeout 36000s;
        '';
      };

      # Admin Console websocket
      "^~ /lool/adminws" = {
        proxyPass = "http://localhost:9980";
        extraConfig = ''
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection "Upgrade";
          proxy_set_header Host $host;
          proxy_read_timeout 36000s;
        '';
      };
    };
  };

  security.acme.certs."nextcloud.manuelbaerenz.de".extraDomainNames = [
    "office.manuelbaerenz.de"
  ];
}
