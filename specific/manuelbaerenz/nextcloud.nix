{ config, pkgs, ... }:

{
  imports = [
    ./nextcloud/collabora.nix
  ];

  users.users.nextcloud.uid = 1001; # Ugly fix because somehow nextcloud ended up having the same uid like turion

  services.nextcloud = {
    # FIXME to do: redis caching & imaginary
    # FIXME onlyoffice with https://carjorvaz.com/posts/the-holy-grail-nextcloud-setup-made-easy-by-nixos/
    enable = true;
    config = {
      adminpassFile = "/var/lib/nextcloud/nextcloud_admin_pass.txt";
      dbtype = "pgsql";
      dbhost = "/run/postgresql";
      dbuser = "nextcloud"; # FIXME I shouldn't need this
    };
    # database.createLocally = true;

    settings = {
      trusted_domains = [
        "www.manuelbaerenz.de"
        "manuelbaerenz.de"
        "89.238.82.209"
      ];
      trusted_proxies = [
        "46.183.103.8"
      ];
      default_phone_region = "DE";

      trashbin_retention_obligation = "auto, 7";
      # Workaround from https://github.com/nextcloud/desktop/issues/5094: Bulk upload makes the client hang?
      "bulkupload.enabled" = false;

      # Shotgun debugging for the login problems
      # https://help.nextcloud.com/t/session-token-credentials-are-invalid-a-few-minutes-after-login/178736
      # https://docs.nextcloud.com/server/26/admin_manual/configuration_server/config_sample_php_parameters.html?highlight=timeout
      session_relaxed_expiry = true;
      remember_login_cookie_lifetime = 60 * 60 * 24 * 1000;
      session_lifetime = 60 * 60 * 24 * 10;
      auto_logout = false;

      # See https://docs.nextcloud.com/server/28/admin_manual/configuration_server/background_jobs_configuration.html#parameters
      maintenance_window_start = 1;
    };
    hostName = "nextcloud.manuelbaerenz.de";
    package = pkgs.nextcloud29;
    phpExtraExtensions = all: (with all; [ pdlib bz2 ]);
    phpOptions = {
      max_input_time = "3600";
      max_execution_time = "3600";
      # Recommended in https://discourse.nixos.org/t/nixos-centric-nextcloud-paid-support-wanted/20137
      "opcache.jit" = "tracing";
      "opcache.jit_buffer_size" = "100M";
      # recommended by nextcloud admin overview
      "opcache.interned_strings_buffer" = "16";
    };
    maxUploadSize = "10G";
    autoUpdateApps = {
      enable = true;
      startAt = "11:00:00";
    };
    https = true;
  };

  # This should do the same as database.createLocally, but for some reason this works, and database.createLocally doesn't
  services.postgresql = {
    # Copied & adapted from nixpkgs
    enable = true;
    ensureDatabases = [ "nextcloud" ];
    ensureUsers = [{
      name = "nextcloud";
      ensureDBOwnership = true;
    }];
  };

  services.postgresqlBackup = {
    enable = true;
    databases = [ "nextcloud" ];
  };

  services.nginx = {
    virtualHosts."nextcloud.manuelbaerenz.de" = {
      enableACME = true;
      forceSSL = true;
      extraConfig = ''
        access_log syslog:server=unix:/dev/log;
        fastcgi_request_buffering off;
        proxy_max_temp_file_size 10000m;
      '';
    };
  };

  # Remove after update
  nixpkgs.config.permittedInsecurePackages = [
    "nextcloud-27.1.11"
  ];
}
