gilgamesch-chor-epos:

let
  domain = "www.${domainShort}";
  domainShort = "gilgamesch-chor-epos.de";
in
{
  services.nginx =
    {
      virtualHosts.${domain} = {
        useACMEHost = "nextcloud.manuelbaerenz.de";
        forceSSL = true;
        locations."/" = {
          root = gilgamesch-chor-epos.packages."x86_64-linux".site;
        };
      };

      virtualHosts.${domainShort} = {
        useACMEHost = "nextcloud.manuelbaerenz.de";
        forceSSL = true;
        extraConfig = "rewrite ^/(.*)$ https://${domain}/$1 redirect;";
      };
    };

  security.acme.certs."nextcloud.manuelbaerenz.de".extraDomainNames = [
    domain
    domainShort
  ];
}
