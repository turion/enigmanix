{ config, pkgs, ... }:

{
  home = {
    sessionVariables = {
      TMPDIR = "/home/turion/tmpdir";
    };
  };

  programs = {
    git = {
      userEmail = "programming@manuelbaerenz.de";
    };
  };
}
