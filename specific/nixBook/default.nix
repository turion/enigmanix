{ config, lib, pkgs, ... }:

{
  boot.kernel.sysctl = {
    "vm.swappiness" = 10;
  };

  networking.enableB43Firmware = true;

  nixpkgs.config.allowUnfree = true;

  services.libinput.touchpad = {
    disableWhileTyping = true;
    additionalOptions = ''
      Option "RBCornerButton" "3"
    '';
  };

  environment.systemPackages = with pkgs; [
    ecryptfs
    ecryptfs-helper
  ];

  swapDevices =
    [ {
        device = "/dev/sda2";
        randomEncryption.enable = true;
      }
    ];

  security.pam = {
    enableEcryptfs = true;
  };

  home-manager.sharedModules = [
    (import ./home.nix)
  ];

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.03"; # Did you read the comment?
}
