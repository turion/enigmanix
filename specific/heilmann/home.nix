{ config, pkgs, ... }:

{
  programs.git = {
    userEmail = "mbaerenz@heilmannsoftware.de";
  };

  home.packages = with pkgs; [
    awscli
  ];
}
