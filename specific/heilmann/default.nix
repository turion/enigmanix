{ lib, pkgs, ... }:

{
  nix.settings = {
    substituters = [
      "https://nixcache.reflex-frp.org"
      "http://nixcache.heilmannsoftware.net"
    ];
    trusted-public-keys = [
      "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI="
      "nixcache.heilmannsoftware.net:5kPz/A5gVCs8E96y8PlT10sdAJtAQ5n4n3E0j7UXGQA="
    ];
  };

  imports = [
    ./hardware-configuration.nix
  ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.initrd.luks.devices."luks-def27fe1-6fc1-49ae-be2e-b77df1dc40de".device = "/dev/disk/by-uuid/def27fe1-6fc1-49ae-be2e-b77df1dc40de";

  networking.hostName = "heilmann";

  networking.useNetworkd = true;
  systemd.network.wait-online.enable = false;
  networking.networkmanager.wifi.powersave = true;

  services.couchdb = {
    enable = true;
    adminUser = "admin";
    adminPass = "admin";
    bindAddress = "::";
    extraConfig = lib.generators.toINI { } {
      chttpd_auth.timeout = 604800; # one week
      log.writer = "journald"; # to prevent redundant timestamps
    };
  };

  services.earlyoom = {
    enable = true;
    freeMemThreshold = 30;
    freeSwapThreshold = 80;
  };

  fonts = {
    enableDefaultPackages = true;
    fontDir.enable = true;
    packages = with pkgs; [
      dejavu_fonts
    ];
  };

  home-manager.sharedModules = [
    (import ./home.nix)
  ];

  # To ensure system keeps building if work cache is not reachable
  nix.settings.fallback = true;

  system.stateVersion = "23.11";
}
