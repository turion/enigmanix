{ lib, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];

  # See https://discourse.nixos.org/t/touchpad-click-not-working/12276, experiment
  boot.kernelParams = [ "psmouse.synaptics_intertouch=0" ];

  networking.hostName = "thnixpad"; # Define your hostname.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s25.useDHCP = true;
  networking.interfaces.wlp3s0.useDHCP = true;
  networking.interfaces.wwp0s20u4.useDHCP = true;

  home-manager.sharedModules = [
    (import ./home.nix)
  ];

  nix.settings.max-jobs = 1;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?
}
