# Checkliste Installation eines neuen Laptops

1. Beliebiges NixOS über Installer installieren, dabei zukünftigen Hostnamen festlegen
1. Firefox Sync
1. SSH-Schlüssel anlegen & auf Gitlab
1. enigmanix clonen, neuen output anlegen
1. /etc/nixos/hardware-configuration.nix kopieren
1. Notwendige Stellen aus /etc/nixos/configuration.nix adaptieren
1. nixos-rebuild switch
1. Nextcloud konfigurieren, nur die notwendigsten Ordner (vermutlich Organisation)
1. KDE Einstellungen: Key to choose 3rd level Right Ctrl, Hintergrundbilder
1. Wenn Nextcloud erster Sync durch nochmal alle relevanten Ordner
