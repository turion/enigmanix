# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

inputs@{ nixpkgs, ... }:
{ config, pkgs, lib, ... }:

{
  imports = [
  ];

  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true;
  };

  # console uses these settings as well
  services.xserver = {
    xkb = {
      variant = "neo";
      layout = "de";
    };
  };

  # Select internationalisation properties.
  i18n = {
    defaultLocale = "de_DE.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    htop
    lshw
    lsof
    nethogs
    nix-output-monitor
    vim
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.bash.completion.enable = true;
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };
  programs.gnupg.agent = { enable = true; };

  programs.ssh = {
    startAgent = true;
    extraConfig = ''
      AddKeysToAgent=yes
    '';
  };

  programs.command-not-found.enable = false; # Using nix-index instead
  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.turion = {
    isNormalUser = true;
    extraGroups = [
      "docker"
      "networkmanager"
      "wheel"
    ];
    group = "users";
    uid = 1000; # Why on earth did I do that
  };

  home-manager = {
    users.turion = import ./home.nix inputs;
    useGlobalPkgs = true;
  };

  nix = {
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    gc = {
      automatic = true;
      dates     = "Tue,Fri";
      options   = "--delete-older-than 14d";
    };
    daemonIOSchedPriority = 7;
    nixPath = [
      "nixpkgs=${nixpkgs}"
    ];
    settings = {
      trusted-users = [
        "root"
        "turion"
      ];
      auto-optimise-store = true;
    };

    package = pkgs.lix;
  };
}
