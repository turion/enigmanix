{ pkgs, ... }:

{
  programs = {
    git = {
      userEmail = "programming@manuelbaerenz.de";
    };
  };
}
