{ pkgs, config, ...}:

{
  systemd.user = {
    services.signal-snapshot = {
      Unit = {
        Description = "Take snapshot of Signal backups";
      };
      Service = {
        WorkingDirectory = "${config.home.homeDirectory}/Nextcloud/Backups/Signal";
        Type = "oneshot";
        ExecStart = let
          script = pkgs.writeShellScript "snapshotBackup" ''
            ${pkgs.coreutils}/bin/ls *.backup -1t | ${pkgs.coreutils}/bin/head -n 1 | ${pkgs.findutils}/bin/xargs cp -t Snapshots
            '';
          in "${script}";
      };
    };

    timers.signal-snapshot = {
      Unit = {
        Description = "Take snapshot of Signal backups timer";
      };
      Timer = {
        OnCalendar = "0 */3 months";
        Unit = "signal-snapshot.service";
      };
      Install = {
        WantedBy = [ "timers.target" ];
      };
    };

    services.signal-backup = {
      Unit = {
        Description = "Delete old Signal backups";
      };
      Service = {
        WorkingDirectory = "${config.home.homeDirectory}/Nextcloud/Backups/Signal";
        Type = "oneshot";
        ExecStart = let
          script = pkgs.writeShellScript "deleteOlderFiles" ''
            # Delete all but the three newest *.backup files
            ${pkgs.coreutils}/bin/ls *.backup -1t | ${pkgs.coreutils}/bin/tail -n +4 | ${pkgs.findutils}/bin/xargs rm -f
            # Delete temporary .backup* files that were accidentally uploaded
            ${pkgs.coreutils}/bin/ls .backup* -1t | ${pkgs.findutils}/bin/xargs rm -f
            '';
          in "${script}";
      };
    };

    timers.signal-backup = {
      Unit = {
        Description = "Delete old Signal backups timer";
      };
      Timer = {
        OnCalendar = "minutely";
        Unit = "signal-backup.service";
      };
      Install = {
        WantedBy = [ "timers.target" ];
      };
    };
  };
}
