{ config, pkgs, ... }:

{
  imports = [
    ../haskell.nix
    ./signal-backups.nix
  ];

  home = {
    sessionVariables = {
      EDITOR     = "codium -w";
      PATH = "$PATH:${config.home.homeDirectory}/.local/bin";
    };

    file.".xprofile".text = ''
      . "$HOME/.profile"
    '';

    file."externalXmodmap".text = ''
      keycode  23 = minus emdash ssharp question NoSymbol U2011 KP_Subtract KP_Subtract hyphen NoSymbol backslash questiondown U1E9E
      keycode  20 = Tab ISO_Left_Tab Tab ISO_Left_Tab Multi_key ISO_Level5_Lock NoSymbol NoSymbol NoSymbol ISO_Level5_Lock Multi_key ISO_Level5_Lock NoSymbol NoSymbol NoSymbol ISO_Level5_Lock
      keycode  51 = ssharp U1E9E udiaeresis Udiaeresis U017F Greek_finalsmallsigma U2212 NoSymbol jot NoSymbol dead_diaeresis dead_abovering
      keycode  34 = dead_acute dead_tilde plus asterisk dead_stroke dead_abovecomma dead_doubleacute NoSymbol dead_breve NoSymbol asciitilde macron
      keycode  35 = ISO_Level3_Shift NoSymbol numbersign apostrophe rightsinglequotemark dead_breve
    '';

    packages = with pkgs; [
      ardour
      ark
      audacity
      calibre
      captive-browser
      chrysalis
      darktable
      element-desktop
      emote
      gimp
      git-imerge
      gparted
      gwenview
      kate
      kdenlive
      kdiff3
      keepassxc
      krename
      krusader
      kstars
      lazygit
      libreoffice
      musescore
      okular
      pavucontrol
      poppler_utils # pdfunite
      scribus
      signal-desktop
      simplescreenrecorder
      (texlive.combine {
        inherit (texlive)
          scheme-medium
          latexmk
          moderncv
          comment
          doublestroke # dsfonts
          fontawesome5
          multirow
          # Needed for cv
          arydshln
          # The following are needed for EMS publication (Quantum Topology)
          pdf14
          nag
          xpatch
          newtx
          xstring
          fontaxes
          tabto-ltx
          zref
          enumitem
          bibtex
          fixme
          minted
        ;
      })
      thunderbird
      ungoogled-chromium
      usbutils
      vlc
      xorg.xev
      xorg.xmodmap
      yt-dlp
    ];
  };

  programs = {
    direnv = {
      enable = true;
      nix-direnv.enable = true;
    };

    firefox = {
      enable = true;
    };

    vscode = {
      enable = true;
      package = pkgs.vscodium;
      haskell = {
        enable = true;
        hie.enable = false;
      };
      extensions = with pkgs.vscode-extensions; [
        bbenoist.nix
        # alefragnani.bookmarks
        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "bookmarks";
            publisher = "alefragnani";
            version = "13.0.1";
            sha256 = "sha256-4IZCPNk7uBqPw/FKT5ypU2QxadQzYfwbGxxT/bUnKdE=";
          };
        })
      ];
    };
  };

  services = {
    nextcloud-client.enable = true;
  };

  # Move back to home.nix once server is sufficiently upgraded
  nix.gc = {
    automatic = true;
  };
}
